# Quentin Raymondaud
# Free to use

import cv2
import numpy as np

discriPath = '../DataSet/EMOJI/PNG_EMOJI_96_32/'
genPath =  '../DataSet/EMOJI/PNG_EMOJI_64_32/'

## Load Img IMREAD_UNCHANGED for 4 channel on png 
img = cv2.imread(discriPath+'smarted.png', cv2.IMREAD_UNCHANGED)

## Img shape = (32, 96, 4) 
print('Img Shape : ' + str(img.shape) )

## Normalize
normImg = np.divide(img, 255) 
print("Original " + str(img[15][45]))
print("Normalized   " + str(normImg[15][45])) 

## RGB to Grayscale 
gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

dim1, dim2, dim3 = img.shape[0], img.shape[1], img.shape[2]
## Example horizontal 
imgRowExample = np.zeros( ( dim1, dim2, dim3 ), dtype = np.uint8)
for i in range(dim1):
    if ( i % 4  == 0 ):
        imgRowExample = np.delete( imgRowExample, i, 0)
        imgRowExample = np.insert( imgRowExample, i, img[i][:,], axis = 0 )

#Example vertical
imgColumnExample = np.zeros( ( dim1, dim2, dim3 ), dtype = np.uint8)
for i in range(dim2):
    if ( i % 4  == 0 ):
        imgColumnExample = np.delete( imgColumnExample, i, 1)
        imgColumnExample = np.insert( imgColumnExample, i, img[:,i], axis = 1 )

## Output img with window name as 'image' 
cv2.imshow('smarted', normImg)  
cv2.imshow('gray smarted', gray)
cv2.imshow('imgRowExample',imgRowExample)
cv2.imshow("imgColumnExample",imgColumnExample)

## user needs to presse a key to
cv2.waitKey(0) 
## Destroys windows on screen 
cv2.destroyAllWindows()  

import IMG

fullImgPath = "../DataSet/EMOJI/PNG_EMOJI_96_32/"

#Example load with IMG module
images = IMG.load_images_from_folder(fullImgPath,0,10)
images2 = IMG.load_images_from_folder(fullImgPath,10,20)

for im in images :
	cv2.imshow("ok",im)
	cv2.waitKey(0)

for im in images2 :
	cv2.imshow("ok",im)
	cv2.waitKey(0)


# Example crop with IMG module
first = IMG.crop_img(images[1],0)
cv2.imshow("first", first ) 
cv2.waitKey(0) 

second = IMG.crop_img( cv2.cvtColor(images[1], cv2.COLOR_RGB2GRAY),1) # Crop a grayscaled copy of original image
cv2.imshow("second", second ) 
cv2.waitKey(0)

third = IMG.crop_img(images[1],2)
cv2.imshow("third", np.divide(third, 255) ) # display a "normalized" (0 -> 255) to (0 -> 1) space crop 
cv2.waitKey(0) 


# Example fusion with IMG module
fourth = IMG.merge_img(third, images[1], 0)
fourth = IMG.merge_img(third, images[1], 1)
cv2.imshow("fourth", fourth) # display a "normalized" (0 -> 255) to (0 -> 1) space crop 
cv2.waitKey(0) 