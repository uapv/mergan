# Python3 Dependencies 

* apt install python3-numpy  
* apt install python3-opencv  

# IMG_PREPROCESSING_BASIC.py

Basic Img processing with python3 OpenCV  

# IMG.py

Img load, crop and merge example on Img

# FKM_Generator.py

Example and use case of IMG.py

# Run it :

* python3 FKM_Generator.py
* Output result in [DataSet/FKM folder](https://gitlab.com/uapv/mergan/-/tree/master/DataSet/FKM)
